# Google Sheets + Node.js/Express
Simple Node.js/Express api that integrates with Google Sheets to read/update data.

## Setup
You need to add a file to the root of this project called "credentials.json". You can find a guide on how to do this in my YouTube tutorial above.

Then run `npm install` to install dependencies.

## Run locally (port 1337)
Run `npm run start`
